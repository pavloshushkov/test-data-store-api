data-store-api

brew install mongodb
sudo mkdir -p /data/db
sudo chown -R `id -un` /data/db
mongod

brew install rabbitmq
brew services start rabbitmq

celery -A data_store_api worker -l info


```
PUT /api/files
Headers [
  {"key":"Content-Disposition",
  "value":"form-data; filename='<filename>'"},

  {"key":"Content-Type",
  "value":"application/octet-stream"}
]

Body [
  <binary file>
]

<filename> -- name of the file that should be uploaded
<binary file> -- the file that should be uploaded
```


```
GET /api/files/<filename>
```
Return the value of the required file. App define where it is stores by itself.