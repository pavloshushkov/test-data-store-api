from __future__ import absolute_import, unicode_literals
from celery import shared_task
from data_store_api.settings import BASE_DIR
import time
import os
import dropbox
from .connections import set_mongo_connection

@shared_task
def upload_file_to_dropbox(filename):
    # the source file
    filepath = (os.path.join(BASE_DIR,  'temp_storage') +'/{}'.format(filename))  # path object, defining the file

    # target location in Dropbox
    target = "/Temp/"  # the target folder
    target_file = target + filename  # the target path and file name

    # Create a dropbox object using an API v2 key
    d = dropbox.Dropbox('TOKEN')

    # open the file and upload it
    with open(filepath, "wb+") as f:
        # upload gives you metadata about the file
        # we want to overwite any previous version of the file
        meta = d.files_upload(f.read(), target_file, mode=dropbox.files.WriteMode("overwrite"))

    # test server sleep
    # time.sleep(20)

    change_file_status(filename)
    os.remove(filepath)

def change_file_status(filename):
    """
    Changes file field "open_to_read" from False to True in MongoDB
    """
    mongo_collection = set_mongo_connection()
    mongo_collection.update_one(
        {"filename": filename},
        {"$set": {
            "open_to_read": True
        }},
    )