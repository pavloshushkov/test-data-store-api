import pymongo



def set_mongo_connection():
    """
    Connect to MongoDB
    """
    mongo_client = pymongo.MongoClient("mongodb://localhost:27017")
    mongo_collection = mongo_client.files_storage.links
    return mongo_collection