from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser
from data_store_api.settings import BASE_DIR
from rest_framework import status
import pymongo
import dropbox
import os
import io


from .tasks import upload_file_to_dropbox

# Create your views here.

class MongoDBConnection(object):
    """
    Create a Connection
    """
    connection = None

    def __get__(self, instance, owner):
        if not self.connection:
            mongo_client = pymongo.MongoClient("mongodb://localhost:27017")
            self.connection = mongo_client.files_storage.links
        return self.connection

class MongoDBStorage():
    """
    access to MongoDB collection
    """
    mongo_collection = MongoDBConnection()


class StorageCreatorView(APIView, MongoDBStorage):
    """
    Responsible for binary files creation
    """
    parser_classes = (FileUploadParser,)


    def put(self, request):
        up_file = request.FILES.get('file')
        if not up_file:
            return Response('No file to upload', status=status.HTTP_400_BAD_REQUEST)

        filename = up_file.name

        if self._check_file_existance_in_db(filename):
            return Response('File with such name already exists', status=status.HTTP_409_CONFLICT)
        _save_file_to_temporary_storage(up_file)

        self._add_to_db(filename)
        upload_file_to_dropbox.delay(filename)

        return Response(status=status.HTTP_201_CREATED)

    def _check_file_existance_in_db(self, filename):
        return self.mongo_collection.find_one({"filename": filename})

    def _add_to_db(self, filename):
        self.mongo_collection.insert_one({
            "filename": filename,
            "storage": "dropbox",
            "open_to_read": False
        })

def _save_file_to_temporary_storage(file_to_save):
    filename = file_to_save.name
    temp_file = (BASE_DIR + '/temp_storage/{}'.format(filename))
    with io.open(temp_file,'wb') as file_write:
        data = file_to_save.read()
        file_write.write(data)


class StorageContentView(APIView, MongoDBStorage):
    def get(self, request, filename):
        file = self.mongo_collection.find_one({"filename": filename})
        if not file:
            return Response('File not found', status=status.HTTP_400_BAD_REQUEST)

        if not file['open_to_read']:
            with open(BASE_DIR + '/temp_storage/' + filename) as file:
                file_data = file.read()
            return Response(file_data, status=status.HTTP_200_OK)

        if file['storage'].lower() == 'dropbox':
            d = dropbox.Dropbox('TOKEN')
            metadata, res = d.files_download('/Temp/' + filename)
            return Response(res.content, status=status.HTTP_200_OK)



