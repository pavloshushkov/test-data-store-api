from django.conf.urls import url, include
from .views import StorageCreatorView,StorageContentView

urlpatterns = [
    url(r'^files$', StorageCreatorView.as_view()),
    url(r'^files/(?P<filename>[\w]+)/$', StorageContentView.as_view()),
]